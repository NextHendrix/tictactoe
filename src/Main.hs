-- NextHendrix -- Noob Tac Toe
module Main where

import           Control.Monad
import           Data.List
import           System.Exit
import           System.IO

main :: IO ()
main = do
  hSetBuffering stdout NoBuffering
  hSetBuffering stdin NoBuffering
  forever $ do
    putStrLn "Welcome to tic tac toe\n\n"
    putStrLn "Numbers line up with the board as follows:"
    showBoard introTable
    putStr "Enter 1 or 2 for single or multiplayer (or q to quit): "
    players <- getLine
    putStrLn "\n\n"
    case players of
      "1" -> onePlayerGame Circle emptyBoard
      "2" -> twoPlayerGame Circle emptyBoard
      "q" -> do
        putStrLn "nice one lads"
        exitSuccess
      _ -> do
        putStr "\nInvalid choice, restarting"
        main

onePlayerGame :: Square -> Board -> IO ()
onePlayerGame s b = putStrLn "TODO: Implement AI" >> main

twoPlayerGame :: Square -> Board -> IO ()
twoPlayerGame s b = do
  unless (checkBoard b) $ do
    showBoard b
    putStr ("\n\nChoose a square for " ++ show s ++ ": ")
    sq <- getLine
    if notElem sq $ show <$> getAvail b
      then do
        putStrLn "Stop testing my type checking fucker, make a move\n\n"
        twoPlayerGame s b
      else twoPlayerGame (un s) (makeMove (read sq :: Int) s b)
  putStrLn (show s ++ " wins!")
  main

testBoard :: Board
testBoard =
  [[Circle, Blank, Blank], [Cross, Circle, Circle], [Blank, Cross, Cross]]

emptyBoard :: Board
emptyBoard =
  [[Blank, Blank, Blank], [Blank, Blank, Blank], [Blank, Blank, Blank]]

data Square
  = Circle
  | Cross
  | Blank
  deriving (Eq)

un :: Square -> Square
un Circle = Cross
un Cross  = Circle
un _      = Blank

type Board = [[Square]]

type Line = [Square]

instance Show Square where
  show Circle = "O"
  show Cross  = "X"
  show _      = " "

introTable :: [[Int]]
introTable = [[1, 2, 3], [4, 5, 6], [7, 8, 9]]

makeLines :: Board -> [Line]
makeLines b = b ++ transpose b ++ getDiags b

getDiags :: Board -> [Line]
getDiags b = (\x -> [x !! y !! y | y <- [0 .. 2]]) <$> [b, reverse b]

showBoard :: Show a => [[a]] -> IO ()
showBoard = mapM_ print

checkLine :: Line -> Maybe Square
checkLine l =
  case l of
    [Circle, Circle, Circle] -> Just Circle
    [Cross, Cross, Cross]    -> Just Cross
    _                        -> Nothing

checkBoard :: Board -> Bool
checkBoard b = not $ all (== Nothing) $ checkLine <$> makeLines b

makeMove :: Int -> Square -> Board -> Board
makeMove n s b = recombine $ (init . fst $ nb) ++ (s : snd nb)
  where
    nb = splitAt n (join b)

recombine :: [Square] -> Board
recombine xs = [take 3 xs, take 3 $ drop 3 xs, drop 6 xs]

getAvail :: Board -> [Int]
getAvail b = (+ 1) <$> elemIndices Blank (join b)
